from django.shortcuts import render

from django.shortcuts import render, HttpResponseRedirect, HttpResponse
from django.contrib.auth import authenticate, get_user_model, login, logout
from django.contrib.auth.models import User
from .forms import UserLoginForm, UserRegestrationForm, UserUpdateForm, PasswordUpdateForm

from django.contrib.auth.decorators import login_required
from django.urls import reverse
from .mixins import AjaxFormMixin


# create your views here

def login_view(AjaxFormMixin):
    if AjaxFormMixin.user.is_authenticated:
        print(AjaxFormMixin)
        return HttpResponseRedirect("/home/")
    

    form = UserLoginForm(AjaxFormMixin.POST or None)
    context = {"form": form, "title": "Login", "tab_title": "Login"}
    if form.is_valid():
        username = form.cleaned_data.get("username", "")
        password = form.cleaned_data.get("password", "")
        user = authenticate(username=username, password=password)
        if user is not None:
            if user.is_active:
                login(AjaxFormMixin, user)
                if "next" in AjaxFormMixin.POST:
                    return HttpResponseRedirect(AjaxFormMixin.POST.get("next"))
                return HttpResponseRedirect("/home/")

            else:
                return HttpResponse("User is not activated")
        else:
            return HttpResponseRedirect("/login/")
    return render(AjaxFormMixin, "form.html", context)


def logout_view(request):
    logout(request)
    print("{} has been logged out.".format(request.user))
    return HttpResponseRedirect(reverse("loginapp:login"))



def register_view(request):
    title = "register"
    form = UserRegestrationForm(request.POST or None)
    context = {"form": form, "title": title, "tab_title":"Register"}
    
    if form.is_valid():
        user = form.save(commit=False)
        password = form.cleaned_data.get("password")
        user.set_password(password)
        user.save()
        
    return render(request, "form.html", context)


@login_required(login_url="/login/")
def all_user_view(request):
    users = User.objects.values("id", "username")
    print(users)
    title = "user list"
    context = {"users": users, "title": title, "tab_title":"Users"}
    return render(request, "list.html", context)


@login_required(login_url="/login/")
def detail_user_view(request, id):
    context = {}

    user = User.objects.get(id=id)
    context["user"] = user
    return render(request, "detail.html", context)


@login_required(login_url="/login/")
def delete_user_view(request, id):
    selecteduser = User.objects.get(id=id)
    context = {}
    if request.user.is_superuser:
        if not selecteduser.is_superuser:
            if request.method == "POST":
                selecteduser.delete()
                return HttpResponseRedirect(reverse("loginapp:users"))

        else:
            context["error"] = "Cannot delete superuser"
            return render(request, "error.html", context)

        context = {
            "user": selecteduser
        }
        return render(request, "user_delete.html", context)
    else:
        context["error"] = "Cannot delete superuser"
        return render(request, "error.html", context)


def update_user_view(request, id):
    user = User.objects.get(id=id)
    title = "Update form"
    if request.user.is_superuser:
        if request.method == 'POST':
            form = UserUpdateForm(request.POST, instance=user)
            update = form.save(commit=False)
            update.save()
            return HttpResponseRedirect("/users/")
        else:
            form = UserUpdateForm(instance=user)
            context = {"form": form, "title": title}
        return render(request, 'form.html', context)
    else:
        context = {"error": "Not authorized to perform updates"}
        return render(request, 'error.html', context)

@login_required(login_url="/login/")
def update_password_view(request, id):
    user = User.objects.get(id=id)
    title = "Update password"
    if request.user.is_superuser:
        if request.method == 'POST':
            form = PasswordUpdateForm(request.POST, instance=user)
            updatep = form.save(commit=False)
            updatep.save()
            return HttpResponseRedirect("/users/")
        else:
            form =PasswordUpdateForm(instance=user)
            context = {"form": form, "title": title}
        return render(request, 'form.html', context)
    else:
        context = {"error": "Not authorized to perform updates"}
        return render(request, 'error.html', context)
#
