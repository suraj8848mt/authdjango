
from django.urls import path

from .views import login_view, logout_view, register_view, all_user_view, delete_user_view,detail_user_view, update_user_view#, update_password_view
from .views import update_password_view
from django.views.generic import TemplateView

app_name = 'loginapp'

urlpatterns = [
    path('',TemplateView.as_view(template_name="main.html"), name="main"),
    path('home/',TemplateView.as_view(template_name="home.html"), name="home"),
    path('login/',login_view, name="login"),
    path('logout/',logout_view, name="logout"),
    path('register/',register_view, name="fillintheblank"),
    path('users/', all_user_view, name="users"),
    path('users/<str:id>/',detail_user_view, name="users_detail"),
    path('delete/<str:id>/', delete_user_view, name="delete_user"),
    path('update/<str:id>/', update_user_view, name="update"),
    path('updatep/<int:id>/', update_password_view, name="password"),
]
