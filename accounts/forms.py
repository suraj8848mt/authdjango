from django import forms
from django.contrib.auth import authenticate, get_user_model

User = get_user_model()

class UserLoginForm(forms.Form):
    username = forms.CharField()
    password = forms.CharField(widget=forms.PasswordInput)

    def clean(self, *args,**kwargs):
        username = self.cleaned_data.get("username")
        password = self.cleaned_data.get("password")

        if password and username:
            user = authenticate(username=username,password=password)
            print(user)
            if user:
                if user.is_active:
                    pass
                else:
                    msg = "User is deactivated"
                    raise forms.ValidationError(msg)
            else:
                msg = "Unable to login with given credentials"
                raise forms.ValidationError(msg)
        else:
            msg = "Please provide valid user name and password"
            raise forms.ValidationError(msg)
        return super(UserLoginForm,self).clean()

class UserRegestrationForm(forms.ModelForm):
    password = forms.CharField(widget=forms.PasswordInput)
    confirm_password = forms.CharField(widget=forms.PasswordInput)

    class Meta:
        model = User
        fields = [
            'username',
            'email',
            'password',
        ]


    def clean_confirm_password(self):
        print(self.cleaned_data)
        password = self.cleaned_data.get("password")
        confirm_password = self.cleaned_data.get("confirm_password")
        if password != confirm_password:
            msg = "password doesn't match"
            raise forms.ValidationError(msg)
        return super(UserRegestrationForm, self).clean()

    def clean_email(self):
        email = self.cleaned_data.get('email') 
        qs = User.objects.filter(email=email) 
        if qs.exists(): 
            raise forms.ValidationError('Email already exists') 
        return email
    
class UserUpdateForm(forms.ModelForm):
    username = forms.CharField(widget=forms.TextInput(attrs={'class':'form-control', 'readonly':'readonly'}))
    first_name = forms.CharField(widget=forms.TextInput(attrs={'class':'form-control'}))
    last_name = forms.CharField(widget=forms.TextInput(attrs={'class':'form-control'}))
    email = forms.CharField(widget=forms.TextInput(attrs={'class':'form-control'}))
    is_staff = forms.RadioSelect()
    is_active = forms.RadioSelect()
    is_superuser = forms.RadioSelect()

    

    class Meta:
        model = User
        fields = [
            "username",
            "first_name",
            "last_name",
            "email",
            "is_staff",
            "is_active",
            "is_superuser",
        ]
class PasswordUpdateForm(forms.ModelForm):
    old_password = forms.CharField(widget=forms.PasswordInput)
    New_password = forms.CharField(widget=forms.PasswordInput)
    confirm_password = forms.CharField(widget=forms.PasswordInput)

    class Meta:
        model = User
        fields = [
            'old_password',
            'New_password',
            'confirm_password'
        ]

    def clean_confirm_password(self, request):
        print(self.cleaned_data)
        old_password = self.cleaned_data.get("password")
        New_password = self.cleaned_data.get("password")
        confirm_password = self.cleaned_data.get("confirm_password")

        if New_password != confirm_password and old_password!=request.user.password:
            msg = "password doesn't match"
            raise forms.ValidationError(msg)
        return super(UserRegestrationForm, self).clean()

